module git.slxh.eu/matrix/window_manager

go 1.18

require (
	github.com/google/go-cmp v0.5.9
	github.com/silkeh/senml v0.0.0-20210422111947-b6df6a0d383f
	github.com/wcharczuk/go-chart/v2 v2.1.0
	gitlab.com/silkeh/cloudburst v0.0.0-20220211194352-a550a8d2aa02
	gitlab.com/silkeh/env v0.1.0
	gitlab.com/silkeh/matrix-bot v0.0.0-20220108152249-8b2d6ecea043
)

require (
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/matrix-org/gomatrix v0.0.0-20210324163249-be2af5ef2e16 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/ugorji/go/codec v1.2.7 // indirect
	golang.org/x/image v0.2.0 // indirect
)
