package main

import (
	"flag"
	"log"
	"strings"
	"time"

	"gitlab.com/silkeh/env"
	matrix "gitlab.com/silkeh/matrix-bot"

	"git.slxh.eu/matrix/window_manager/electricity"
	"git.slxh.eu/matrix/window_manager/notifier"
	"git.slxh.eu/matrix/window_manager/window"
)

func main() {
	var (
		homeserver, userID, token, rooms string // Matrix connection settings
		messageType                      string // Formatting settings
		measurementURL                   string // URL to measurementList
		outsideName                      string // Name of the outside sensor
		stateFile                        string
		interval, lookAhead              time.Duration
		limitLow, limitHigh              float64
		invalidLow, invalidHigh          float64
		priceLow, priceHigh              float64
	)

	flag.StringVar(&homeserver, "homeserver-url", "http://localhost:8008", "Homeserver to connect to.")
	flag.StringVar(&userID, "user-id", "", "User ID to connect with.")
	flag.StringVar(&token, "token", "", "Token to connect with.")
	flag.StringVar(&rooms, "rooms", "", "Comma separated list of rooms from which commands are allowed. All rooms are allowed by default.")
	flag.StringVar(&messageType, "message-type", "m.notice", "Type of message the bot uses.")
	flag.StringVar(&measurementURL, "measurement-url", "http://localhost:7136/measurementList", "URL to cloudburst Measurement endpoint")
	flag.StringVar(&outsideName, "outside-name", "buiten", "Name of the outside sensor")
	flag.StringVar(&stateFile, "state-file", "", "Path to state file. Use this to prevent notifications on restart.")
	flag.DurationVar(&interval, "interval", 5*time.Minute, "Measurement retrieval interval.")
	flag.DurationVar(&lookAhead, "look-ahead", 12*time.Hour, "Time to look ahead for electricity prices.")
	flag.Float64Var(&limitLow, "limit-low", 1, "Limit for lower temperature notifications.")
	flag.Float64Var(&limitHigh, "limit-high", 0, "Limit for higher temperature difference.")
	flag.Float64Var(&invalidLow, "invalid-low", -40, "Lower limit for invalid temperatures.")
	flag.Float64Var(&invalidHigh, "invalid-high", 50, "Upper limit for invalid temperatures.")
	flag.Float64Var(&priceLow, "price-low", 0.05, "Limit for low electricity price.")
	flag.Float64Var(&priceHigh, "price-high", 0.3, "Limit for high electricity price.")

	if err := env.ParseWithFlags(); err != nil {
		log.Fatalf("Error parsing flags/environment variables: %s", err)
	}

	flag.Parse()

	// Check if user is set
	if userID == "" {
		log.Fatal("Error: no user ID")
	}

	// Check if token is set
	if token == "" {
		log.Fatal("Error: no token")
	}

	// Create/connect client
	log.Printf("Connecting to Matrix homeserver at %q as %q", homeserver, userID)
	client, err := matrix.NewClient(homeserver, userID, token,
		&matrix.ClientConfig{MessageType: messageType})
	if err != nil {
		log.Fatalf("Error connecting to Matrix: %s", err)
	}

	// Create notifier/scraper/etc
	notify := notifier.NewNotifier(client, stateFile)
	elCheck := electricity.NewClient(priceLow, priceHigh)
	checker := window.Checker{
		URL:         measurementURL,
		OutsideName: outsideName,
		LimitLow:    limitLow,
		LimitHigh:   limitHigh,
		InvalidLow:  invalidLow,
		InvalidHigh: invalidHigh,
	}

	// Load state for notifier
	if err := notify.Load(); err != nil {
		log.Fatalf("Error loading state: %s", err)
	}

	ticker := time.NewTicker(interval)
	for ; true; <-ticker.C {
		messages, err := checker.Check()
		if err != nil {
			log.Printf("Error checking room temperatures: %s", err)
		}

		elMessages, err := elCheck.CheckGraph(time.Now().Add(lookAhead))
		if err != nil {
			log.Printf("Error checking electricity prices: %s", err)
		}

		messages = append(messages, elMessages...)
		if len(messages) == 0 {
			continue
		}

		for _, r := range strings.Split(rooms, ",") {
			for _, message := range messages {
				if notify.Sent(message) {
					continue
				}

				log.Printf("Sending message to %q", r)

				err = notify.Send(r, message)
				if err != nil {
					log.Printf("Error sending message to %q: %s", r, err)
				}
			}
		}

		if err := notify.Save(); err != nil {
			log.Fatalf("Error storing state: %s", err)
		}
	}
}
