package electricity

import (
	"bytes"
	"fmt"
	"image"
	"image/png"

	"github.com/wcharczuk/go-chart/v2"
	"github.com/wcharczuk/go-chart/v2/drawing"
	"gitlab.com/silkeh/cloudburst/scrapers/easyenergy"
)

var (
	axisColor = drawing.ColorFromAlphaMixedRGBA(0, 0, 0, 64)
	highStyle = chart.Style{
		FillColor:   drawing.ColorFromHex("c11313"),
		StrokeColor: drawing.ColorFromHex("c11313"),
		StrokeWidth: -1,
	}
	lowStyle = chart.Style{
		FillColor:   drawing.ColorFromHex("13c158"),
		StrokeColor: drawing.ColorFromHex("13c158"),
		StrokeWidth: -1,
	}
	normalStyle = chart.Style{
		FillColor:   drawing.ColorFromHex("cccccc"),
		StrokeColor: drawing.ColorFromHex("cccccc"),
		StrokeWidth: -1,
	}
)

func getStyle(price, low, high float64) chart.Style {
	switch {
	case price <= low:
		return lowStyle
	case price >= high:
		return highStyle
	default:
		return normalStyle
	}
}

func graph(prices []easyenergy.EnergyTariff, low, high float64) (image.Image, error) {
	values := make([]chart.Value, len(prices))
	for i, price := range prices {
		values[i].Label = price.Time.Local().Format("15:04")
		values[i].Value = price.TariffUsage
		values[i].Style = getStyle(price.TariffUsage, low, high)
	}

	sbc := chart.BarChart{
		Title:        fmt.Sprintf("Energieprijs op %s", prices[0].Time.Local().Format("2006-01-02")),
		Bars:         values,
		Height:       512,
		BarWidth:     30,
		BarSpacing:   8,
		UseBaseValue: true,
		BaseValue:    0.0,
		Background: chart.Style{
			Padding: chart.Box{
				Top:    50,
				Bottom: 20,
				Right:  1,
			},
		},
		XAxis: chart.Style{
			FontSize:    9,
			StrokeWidth: 1,
			StrokeColor: axisColor,
			FillColor:   axisColor,
		},
		YAxis: chart.YAxis{
			Name: "Price in Eurocents",
			ValueFormatter: func(v any) string {
				return fmt.Sprintf("€ %.3f", v)
			},
			Style: chart.Style{
				TextWrap:    chart.TextWrapRune,
				Hidden:      false,
				StrokeWidth: 1,
				StrokeColor: axisColor,
				FillColor:   axisColor,
			},
		},
	}

	buffer := bytes.NewBuffer(nil)
	err := sbc.Render(chart.PNG, buffer)
	if err != nil {
		return nil, err
	}

	return png.Decode(buffer)
}
