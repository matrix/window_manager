package electricity

import (
	"fmt"
	"time"

	"gitlab.com/silkeh/cloudburst/scrapers/easyenergy"

	"git.slxh.eu/matrix/window_manager/notifier"
)

const (
	timeFormat   = "2006-01-02 15:04:05 MST"
	dayStartHour = 0
)

type Client struct {
	client    *easyenergy.Client
	priceLow  float64
	priceHigh float64
}

func NewClient(priceLow, priceHigh float64) *Client {
	return &Client{
		client:    easyenergy.NewClient(nil),
		priceLow:  priceLow,
		priceHigh: priceHigh,
	}
}

func convertDate(date time.Time) (start, end time.Time) {
	start = time.Date(date.Year(), date.Month(), date.Day(), dayStartHour, 0, 0, 0, date.Location())
	end = time.Date(date.Year(), date.Month(), date.Day()+1, dayStartHour, 0, 0, 0, date.Location())

	return
}

func (c *Client) checkPrices(start, end time.Time) (tariffs []easyenergy.EnergyTariff, low bool, err error) {
	prices, err := c.client.Electricity(start, end)
	if err != nil {
		return nil, false, fmt.Errorf("error retrieving electricity prices: %w", err)
	}

	for _, p := range prices {
		if p.TariffUsage <= c.priceLow {
			return prices, true, nil
		}
	}

	return prices, false, nil
}

func (c *Client) CheckGraph(date time.Time) ([]notifier.Message, error) {
	start, end := convertDate(date)

	prices, low, err := c.checkPrices(start, end)
	if err != nil {
		return nil, err
	}

	if !low {
		return nil, nil
	}

	img, err := graph(prices, c.priceLow, c.priceHigh)
	if err != nil {
		return nil, err
	}

	return []notifier.Message{
		&notifier.MarkdownMessage{
			Key:     fmt.Sprintf("electricity_price_%v", start.Unix()),
			Content: fmt.Sprintf("\u26A1 Lage energieprijs op %s:", start.Format("2006-01-02")),
		},
		&notifier.Image{
			Description: fmt.Sprintf("Energy price for %s", date.Format("2006-01-02")),
			MIMEType:    "image/png",
			Image:       img,
			Repeat:      false,
		},
	}, nil
}

func (c *Client) Check(searchAhead time.Duration) ([]notifier.Message, error) {
	prices, err := c.client.Electricity(time.Now(), time.Now().Add(searchAhead))
	if err != nil {
		return nil, fmt.Errorf("error retrieving electricity prices: %w", err)
	}

	var messages []notifier.Message
	for _, p := range prices {
		if p.TariffUsage >= c.priceLow {
			continue
		}

		messages = append(messages, &notifier.MarkdownMessage{
			Key: fmt.Sprintf("electricity_price_%v", p.Time.Unix()),
			Content: fmt.Sprintf("\u26A1 Lage energieprijs om %s: € %.3f\n\n",
				p.Time.Local().Format(timeFormat), p.TariffUsage),
		})
	}

	return messages, nil
}
