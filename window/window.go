package window

import (
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"strings"
	"time"

	"github.com/silkeh/senml"

	"git.slxh.eu/matrix/window_manager/notifier"
)

const (
	openText      = "\U0001F976 Open"  // Cold face
	closeText     = "\U0001F975 Sluit" // Hot face
	invalidAction = "invalid"
	format        = "%s het raam in de %s: het temperatuurverschil is %.1f°C\n\n"
	formatInvalid = "\U0001F321\uFE0F Ongeldige temperatuur: %s is %.1f°C\n\n"
	formatUnknown = "\U0001F321\uFE0F Geen temperatuurwaarde voor %q\n\n"
)

// invalidValue returns a value that is different every hour.
func invalidValue() any {
	return time.Now().Unix() / 3600
}

type Action struct {
	Host  string
	Label string
	Value float64
	Sent  bool
	Time  time.Time
}

func (a *Action) Message() *notifier.MarkdownMessage {
	return &notifier.MarkdownMessage{
		Key:     a.Host,
		Value:   a.Label,
		Content: fmt.Sprintf(format, a.Label, a.Host, a.Value),
	}
}

type Checker struct {
	URL         string
	OutsideName string
	LimitLow    float64
	LimitHigh   float64
	InvalidLow  float64
	InvalidHigh float64
}

func (c *Checker) getMeasurements() ([]senml.Measurement, error) {
	// Retrieve the measurementList
	resp, err := http.Get(c.URL)
	if err != nil {
		return nil, fmt.Errorf("error retrieving measurementList from %q: %w", c.URL, err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading request data: %w", err)
	}

	measurementList, err := senml.DecodeJSON(body)
	if err != nil {
		return nil, fmt.Errorf("error decoding JSON: %s", err)
	}

	return measurementList, nil
}

func (c *Checker) Check() ([]notifier.Message, error) {
	measurementList, err := c.getMeasurements()
	if err != nil {
		return nil, err
	}

	outside := c.outside(measurementList)
	if outside == nil {
		return []notifier.Message{&notifier.MarkdownMessage{
			Key:     c.OutsideName,
			Value:   invalidValue(),
			Content: fmt.Sprintf(formatUnknown, c.OutsideName),
		}}, nil
	}

	return c.messages(measurementList, outside), nil
}

func (c *Checker) outside(list []senml.Measurement) *senml.Value {
	for _, m := range list {
		name := splitLast(m.Attrs().Name, ":")

		v, ok := m.(*senml.Value)
		a := m.Attrs().Unit == senml.Celsius
		b := name[0] == c.OutsideName
		if ok && a && b {
			return v
		}
	}

	return nil
}

func (c *Checker) messages(list []senml.Measurement, outside *senml.Value) []notifier.Message {
	messages := make([]notifier.Message, 0, len(list))
	for _, m := range list {
		v, ok := m.(*senml.Value)
		if !ok || v.Unit != senml.Celsius {
			continue
		}

		msg := c.message(outside, v)
		if msg == nil {
			continue
		}

		messages = append(messages, msg)
	}

	return messages
}

func getHost(v *senml.Value) (string, bool) {
	host := splitLast(v.Name, ":")[0]
	if net.ParseIP(host) != nil {
		return "", false
	}

	return host, true
}

func (c *Checker) message(outside, v *senml.Value) *notifier.MarkdownMessage {
	tDiff := v.Value - outside.Value
	host, ok := getHost(v)
	if !ok {
		return nil
	}

	var action string
	switch {
	case v.Value < c.InvalidLow, v.Value > c.InvalidHigh:
		action = invalidAction
	case v == outside:
		action = ""
	case tDiff >= c.LimitLow:
		action = openText
	case tDiff <= -c.LimitHigh:
		action = closeText
	}

	switch action {
	case "":
		return nil
	case invalidAction:
		return &notifier.MarkdownMessage{
			Key:     host,
			Value:   invalidValue(),
			Content: fmt.Sprintf(formatInvalid, host, v.Value),
		}
	default:
		return &notifier.MarkdownMessage{
			Key:     host,
			Value:   action,
			Content: fmt.Sprintf(format, action, host, tDiff),
		}
	}
}

func splitLast(s, sep string) []string {
	i := strings.LastIndex(s, sep)
	if i < 0 {
		return []string{s}
	}
	return []string{s[:i], s[i+1:]}
}
