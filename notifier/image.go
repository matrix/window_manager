package notifier

import (
	"bytes"
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"image"
	"image/png"

	bot "gitlab.com/silkeh/matrix-bot"
)

type ImageInfo struct {
	MIMEType string `json:"mimetype"`
	Width    int    `json:"w"`
	Height   int    `json:"h"`
}

type ImageContent struct {
	Body string    `json:"body"`
	Type string    `json:"msgtype"`
	URL  string    `json:"url"`
	Info ImageInfo `json:"info"`
}

type Image struct {
	Description string
	MIMEType    string
	Image       image.Image
	Repeat      bool
}

func (i *Image) ID() string {
	return i.Description
}

func (i *Image) Resend() bool {
	return i.Repeat
}

func (i *Image) CompareValue() any {
	checksum := sha256.New()
	bounds := i.Image.Bounds()
	buf := make([]byte, 16)

	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		for x := bounds.Min.X; x < bounds.Max.X; x++ {
			r, g, b, a := i.Image.At(x, y).RGBA()

			binary.LittleEndian.PutUint32(buf[0:4], r)
			binary.LittleEndian.PutUint32(buf[4:8], g)
			binary.LittleEndian.PutUint32(buf[8:12], b)
			binary.LittleEndian.PutUint32(buf[12:16], a)

			checksum.Write(buf)
		}
	}

	return hex.EncodeToString(checksum.Sum(nil))
}

func (i *Image) File() (data []byte, mimeType string, upload bool) {
	var buf bytes.Buffer

	switch i.MIMEType {
	case "image/png":
		err := png.Encode(&buf, i.Image)
		if err != nil {
			panic(err)
		}
	default:
		panic("unsupported mimetype: " + i.MIMEType)
	}

	return buf.Bytes(), i.MIMEType, true
}

func (i *Image) Message(fileURL string) *bot.Message {
	return bot.NewImageMessage(i.MIMEType, fileURL, i.Description,
		i.Image.Bounds().Dx(), i.Image.Bounds().Dy(), 0)
}
