package notifier

import (
	"bytes"
	"encoding/json"
	"os"

	"github.com/google/go-cmp/cmp"

	bot "gitlab.com/silkeh/matrix-bot"
)

type stateEntry struct {
	Message *bot.Message
	Value   any
}

func newStateEntry(m Message, uri string) stateEntry {
	return stateEntry{Message: m.Message(uri), Value: m.CompareValue()}
}

type Notifier struct {
	client    *bot.Client
	state     map[string]stateEntry
	stateFile string
}

func NewNotifier(client *bot.Client, stateFile string) *Notifier {
	return &Notifier{
		client:    client,
		state:     make(map[string]stateEntry),
		stateFile: stateFile,
	}
}

func (n *Notifier) Sent(m Message) bool {
	v, ok := n.state[m.ID()]

	return !m.Resend() && ok && equal(v, newStateEntry(m, ""))
}

func (n *Notifier) Send(roomID string, m Message) error {
	if n.Sent(m) {
		return nil
	}

	uri, err := n.uploadFile(m)
	if err != nil {
		return err
	}

	_, err = n.client.NewRoom(roomID).SendMessage(m.Message(uri))
	if err != nil {
		return err
	}

	n.state[m.ID()] = newStateEntry(m, uri)

	return nil
}

func (n *Notifier) uploadFile(m Message) (string, error) {
	data, mimeType, upload := m.File()
	if !upload {
		return "", nil
	}

	return n.client.Upload(bytes.NewReader(data), mimeType, len(data))
}

func (n *Notifier) Load() error {
	file, err := os.Open(n.stateFile)
	if err != nil {
		if os.IsNotExist(err) {
			return nil
		}

		return err
	}

	return json.NewDecoder(file).Decode(&n.state)
}

func (n *Notifier) Save() error {
	file, err := os.OpenFile(n.stateFile, os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		return err
	}

	return json.NewEncoder(file).Encode(n.state)
}

func equal(a, b stateEntry) bool {
	return cmp.Equal(a.Value, b.Value)
}
