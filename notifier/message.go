package notifier

import (
	"time"

	bot "gitlab.com/silkeh/matrix-bot"
)

type Message interface {
	ID() string
	Resend() bool
	File() ([]byte, string, bool)
	CompareValue() any
	Message(fileURL string) *bot.Message
}

type MarkdownMessage struct {
	Key     string
	Value   interface{}
	Content string
	Repeat  bool
	Sent    time.Time
}

func (m *MarkdownMessage) ID() string {
	return m.Key
}

func (m *MarkdownMessage) Resend() bool {
	return m.Repeat
}

func (m *MarkdownMessage) CompareValue() any {
	if m.Value == nil {
		return m.Content
	}

	return m.Value
}

func (m *MarkdownMessage) File() ([]byte, string, bool) {
	return nil, "", false
}

func (m *MarkdownMessage) Message(_ string) *bot.Message {
	m.Sent = time.Now()

	msg := bot.NewMarkdownMessage(m.Content)
	msg.MsgType = "m.notice"

	return msg
}
